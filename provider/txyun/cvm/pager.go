package cvm

import (
	"github.com/infraboard/mcube/flowcontrol/tokenbucket"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	cvm "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/cvm/v20170312"

	"gitee.com/go-course/cmdb/apps/host"
)

func newPager(size int, operater *CVMOperater, reqPs int) *pager {
	req := cvm.NewDescribeInstancesRequest()
	req.Limit = common.Int64Ptr(int64(size))

	return &pager{
		size:     size,
		operater: operater,
		number:   1,
		log:      zap.L().Named("CVM Pager"),
		req:      req,
		tb:       tokenbucket.NewBucketWithRate(1/float64(reqPs), 1),
	}
}

type pager struct {
	size     int
	number   int
	total    int64
	operater *CVMOperater
	req      *cvm.DescribeInstancesRequest
	log      logger.Logger
	tb       *tokenbucket.Bucket
}

// 默认接口请求频率限制：40次/秒。
func (p *pager) Next() *host.PagerResult {
	result := host.NewPagerResult()

	// 没调用一次，就需要构造一个翻页的请求
	resp, err := p.operater.Query(p.nextReq())
	if err != nil {
		result.Err = err
		return result
	}

	// 完成一页请求过后，需要修改Total
	p.total = resp.Total
	p.log.Debugf("get %d hosts", len(resp.Items))

	result.Data = resp
	result.HasNext = p.hasNext()

	// 翻页
	p.number++

	return result
}

func (p *pager) nextReq() *cvm.DescribeInstancesRequest {
	// 生成请求的时候, 现获取速率令牌, 等待一个可用的令牌
	p.tb.Wait(1)

	// 这里要添加翻页的逻辑
	p.log.Debugf("请求第%d页数据", p.number)
	p.req.Offset = common.Int64Ptr(p.offset())
	return p.req
}

// 更加total的数据，已经当前请求的数量，来解决是否还有下一页
func (p *pager) hasNext() bool {
	return int64(p.number*p.size) < p.total
}

func (p *pager) offset() int64 {
	return int64((p.number - 1) * p.size)
}
