package task

import (
	"net/http"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/infraboard/mcube/http/request"
	"github.com/rs/xid"
)

var (
	validate = validator.New()
)

const (
	AppName = "task"
)

func NewTaskFromReq(req *CreateTaskRequst) (*Task, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	return &Task{
		Type:         req.Type,
		DryRun:       req.DryRun,
		Id:           xid.New().String(),
		SecretId:     req.SecretId,
		ResourceType: req.ResourceType,
		Region:       req.Region,
		Timeout:      int32(req.Timeout),
	}, nil
}

func (req *CreateTaskRequst) Validate() error {
	// 自定义校验
	return validate.Struct(req)
}

func (t *Task) UpdateSecretDesc(sd string) {
	t.SecretDescription = sd
}

func (t *Task) Run() {
	t.StartAt = time.Now().UnixMilli()
	t.Status = Status_RUNNING
}

func (t *Task) Failed(message string) {
	t.EndAt = time.Now().UnixMilli()
	t.Status = Status_FAILED
	t.Message = message
}

// 判断全部成功，还是部分成功
func (t *Task) Completed() {
	t.EndAt = time.Now().UnixMilli()
	if t.Status != Status_FAILED {
		if t.TotalFailed == 0 {
			t.Status = Status_SUCCESS
		} else {
			t.Status = Status_WARNING
		}
	}
}

func (s *Task) AddDetail(d *Record) {
	if d.IsSuccess {
		s.TotalSucceed++
	} else {
		s.TotalFailed++
	}
}

func NewSyncFailedRecord(taskId, instanceId, instanceName, message string) *Record {
	return &Record{
		TaskId:     taskId,
		InstanceId: instanceId,
		Name:       instanceName,
		Message:    message,
		IsSuccess:  false,
	}
}

func NewSyncSucceedRecord(taskId, instanceId, instanceName string) *Record {
	return &Record{
		TaskId:     taskId,
		InstanceId: instanceId,
		Name:       instanceName,
		IsSuccess:  true,
	}
}

func NewTaskSet() *TaskSet {
	return &TaskSet{
		Items: []*Task{},
	}
}

func (r *TaskSet) Add(item *Task) {
	r.Items = append(r.Items, item)
}

func NewDefaultTask() *Task {
	return &Task{}
}

func NewRecordSet() *RecordSet {
	return &RecordSet{
		Items: []*Record{},
	}
}

func (r *RecordSet) Add(item *Record) {
	r.Items = append(r.Items, item)
}

func NewDefaultTaskRecord() *Record {
	return &Record{}
}

func NewCreateTaskRequst() *CreateTaskRequst {
	return &CreateTaskRequst{}
}

func NewQueryTaskRequestFromHTTP(r *http.Request) *QueryTaskRequest {
	qs := r.URL.Query()

	kw := qs.Get("keywords")

	return &QueryTaskRequest{
		Page:     request.NewPageRequestFromHTTP(r),
		Keywords: kw,
	}
}

func NewQueryTaskRecordRequest(id string) *QueryTaskRecordRequest {
	return &QueryTaskRecordRequest{
		TaskId: id,
	}
}

func NewDescribeTaskRequestWithId(id string) *DescribeTaskRequest {
	return &DescribeTaskRequest{
		Id: id,
	}
}
