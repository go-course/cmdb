package host

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitee.com/go-course/cmdb/apps/resource"
	"github.com/go-playground/validator/v10"
	"github.com/imdario/mergo"
	request "github.com/infraboard/mcube/http/request"
	pb_request "github.com/infraboard/mcube/pb/request"
)

const (
	AppName = "host"
)

var (
	validate = validator.New()
)

func (h *Host) GenHash() error {
	hash := sha1.New()

	b, err := json.Marshal(h.Information)
	if err != nil {
		return err
	}
	hash.Write(b)
	h.Base.ResourceHash = fmt.Sprintf("%x", hash.Sum(nil))

	b, err = json.Marshal(h.Describe)
	if err != nil {
		return err
	}
	hash.Reset()
	hash.Write(b)
	h.Base.DescribeHash = fmt.Sprintf("%x", hash.Sum(nil))
	return nil
}

func (d *Describe) LoadKeyPairNameString(str string) {
	if str != "" {
		d.KeyPairName = strings.Split(str, ",")
	}

}

func (d *Describe) KeyPairNameToString() string {
	return strings.Join(d.KeyPairName, ",")
}

func (d *Describe) LoadSecurityGroupsString(str string) {
	if str != "" {
		d.SecurityGroups = strings.Split(str, ",")
	}
}

func (d *Describe) SecurityGroupsToString() string {
	return strings.Join(d.SecurityGroups, ",")
}

func (req *DescribeHostRequest) Where() (string, interface{}) {
	switch req.DescribeBy {
	case DescribeBy_HOST_ID:
		return "id = ?", req.Value
	default:
		return "", nil
	}
}

func NewDefaultHost() *Host {
	return &Host{
		Base: &resource.Base{},
		Information: &resource.Information{
			Tags: map[string]string{},
		},
		ReleasePlan: &resource.ReleasePlan{},
		Describe:    &Describe{},
	}
}

func NewDescribeHostRequestById(id string) *DescribeHostRequest {
	return &DescribeHostRequest{
		DescribeBy: DescribeBy_HOST_ID,
		Value:      id,
	}
}

func NewHostSet() *HostSet {
	return &HostSet{
		Items: []*Host{},
	}
}

func (s *HostSet) Add(item *Host) {
	s.Items = append(s.Items, item)
}

func (req *UpdateHostRequest) Validate() error {
	return validate.Struct(req)
}

func (h *Host) Put(req *UpdateHostData) {
	// 提前保留老的hash
	oldRH, oldDH := h.Base.ResourceHash, h.Base.DescribeHash

	// 对象更新完成后 重新计算Hash
	h.Information = req.Information
	h.Describe = req.Describe
	h.Information.UpdateAt = time.Now().UnixMilli()
	h.GenHash()

	// 更新后的Hash和之前的Hash是否一致，来判断该对象是否需要更新
	if h.Base.ResourceHash != oldRH {
		h.Base.ResourceHashChanged = true
	}
	if h.Base.DescribeHash != oldDH {
		h.Base.DescribeHashChanged = true
	}
}

func (h *Host) Patch(req *UpdateHostData) error {
	oldRH, oldDH := h.Base.ResourceHash, h.Base.DescribeHash

	// patch information
	err := mergo.MergeWithOverwrite(h.Information, req.Information)
	if err != nil {
		return err
	}

	err = mergo.MergeWithOverwrite(h.Describe, req.Describe)
	if err != nil {
		return err
	}

	h.Information.UpdateAt = time.Now().UnixMilli()
	h.GenHash()

	if h.Base.ResourceHash != oldRH {
		h.Base.ResourceHashChanged = true
	}
	if h.Base.DescribeHash != oldDH {
		h.Base.DescribeHashChanged = true
	}

	return nil
}

func (h *Host) ShortDesc() string {
	return h.Information.PrivateIPToString()
}

func NewQueryHostRequestFromHTTP(r *http.Request) *QueryHostRequest {
	qs := r.URL.Query()

	return &QueryHostRequest{
		Page:     request.NewPageRequestFromHTTP(r),
		Keywords: qs.Get("keywords"),
	}
}

func NewDescribeHostRequestWithID(id string) *DescribeHostRequest {
	return &DescribeHostRequest{
		DescribeBy: DescribeBy_HOST_ID,
		Value:      id,
	}
}

func NewDeleteHostRequestWithID(id string) *ReleaseHostRequest {
	return &ReleaseHostRequest{
		Id:          id,
		ReleasePlan: &resource.ReleasePlan{},
	}
}

func NewUpdateHostDataByIns(ins *Host) *UpdateHostData {
	return &UpdateHostData{
		Information: ins.Information,
		Describe:    ins.Describe,
	}
}

func NewUpdateHostRequest(id string) *UpdateHostRequest {
	return &UpdateHostRequest{
		Id:             id,
		UpdateMode:     pb_request.UpdateMode_PUT,
		UpdateHostData: &UpdateHostData{},
	}
}

func NewPatchHostRequest(id string) *UpdateHostRequest {
	return &UpdateHostRequest{
		Id:             id,
		UpdateMode:     pb_request.UpdateMode_PATCH,
		UpdateHostData: &UpdateHostData{},
	}
}
