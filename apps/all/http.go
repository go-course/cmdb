package all

import (
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	// _ "gitee.com/go-course/cmdb/apps/book/http"
	_ "gitee.com/go-course/cmdb/apps/host/http"
	_ "gitee.com/go-course/cmdb/apps/resource/http"
	_ "gitee.com/go-course/cmdb/apps/secret/http"
	_ "gitee.com/go-course/cmdb/apps/task/http"
)
